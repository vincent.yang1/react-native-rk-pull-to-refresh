'use strict';
import React from 'react';
import ListView from "deprecated-react-native-listview";


import {ScrollView,View} from 'react-native';
import Pullable from '../local/Pullable';

//ScrollView 暂时没有找到比较好的方法去判断时候滚动到顶部，
//所以这里用ListView配合ScrollView进行使用
export default  class PullScrollView extends Pullable {

    getScrollable=()=> {

        return (
            <ListView
                ref={(c) => {this.scroll = c;}}
                renderRow={this.renderRow}
                onScroll={event=>{
                    console.log(event.nativeEvent.contentOffset.y)
                    if(event.nativeEvent.contentOffset.y === 0) {
                    }
                }}
                style={this.props.style}
                onMomentumScrollEnd={this.props.onMomentumScrollEnd}
                onResponderRelease={(e, gestureState)=>{
                    console.log(e)
                    console.log(gestureState)
                    // this.onPullStateChange(e.nativeEvent.locationY*2)
                    this.onPanResponderRelease()
                }}
                onScrollEndDrag={e=>console.log(e)}

                dataSource={new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}).cloneWithRows([])}
                enableEmptySections={true}
                removeClippedSubviews={false}
                renderHeader={this._renderHeader}/>
        );
    }

    renderRow = (rowData, sectionID, rowID, highlightRow) => {
        return <View/>
    }

    _renderHeader = () => {
        return (
            <View
                scrollEnabled={false}>
                {this.props.children}
            </View>
        )
    }

}



